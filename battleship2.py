from random import randint
letters=["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]

def getRow (l):
    l=l.upper()
    if(l=="A"):
        return 0
    elif(l=="B"):
        return 1
    elif(l=="C"):
        return 2
    elif(l=="D"):
        return 3
    elif(l=="E"):
        return 4
    elif(l=="F"):
        return 5
    elif(l=="G"):
        return 6
    elif(l=="H"):
        return 7
    elif(l=="I"):
        return 8
    elif(l=="J"):
        return 9
    


def MakeBoard():
    
    
    board=[]
    for i in range(10):
        temp=[]
        for j in range(10):
            temp.append("[ ]")
        board.append(temp)
        
    return board
UserGuesses=MakeBoard()
def PrintBoard (board):
    print(" 1  2  3  4  5  6  7  8  9  10 ")
    for r in range (10):
        print(letters[r],end="")
        for c in range (10):
            print (board [r] [c],end="")
        print()
PrintBoard(UserGuesses)
UserShips=MakeBoard()

CompShips=MakeBoard()

CompGuesses=MakeBoard()

def PlaceShips (length, UserShips):
    while True:
        PrintBoard(UserShips)
        x=input("Where will you place your ship?")
        col=int(x[1])-1
        row=getRow(x[0])
        direction=input("Do you want to place it right or down?").lower()
        A=True

        if direction=="right":  
            if length+col>11:
                A=False 
            for i in range(length):
                if UserShips[row][col+i]!="[ ]":
                    A=False
                    print("Your ship will hit another ship. You can't go there!")
            if A:
                for i in range(length):
                    UserShips[row][col+i]="[y]"
                return    
        if direction=="down":
            if length+row>11:
                A=False
            for i in range(length):
                if UserShips[row+i][col]!="[ ]":
                    A=False
                    print("Your ship will hit another ship. You can't go there!")
            if A:
                for i in range(length):
                    UserShips[row+i][col]="[y]"
                    
            

PlaceShips(5, UserShips)
PlaceShips(4, UserShips)
PlaceShips(3, UserShips)
PlaceShips(2, UserShips)
    
    
#    
#     for f in range(5):
#         while True:
#             row=randint(0,9)
#         col=randint(0,9)
#         if CompShips[row][col]=="[ ]":
#             CompShips[row][col]="[y]"
#             break
PrintBoard(CompShips) 
UserHits=0
CompHits=0

       
while True:
    PrintBoard(UserGuesses)
    x=input("Where do you want to strike?")
    col=int(x[1])-1
    row=getRow(x[0])
    if CompShips [row] [col]=="[ ]":
        print("You guessed wrong!!!")
        UserGuesses [row] [col]="[-]"
    else:
        UserGuesses [row] [col]="[x]"
        UserHits+=1
        print("You have hit the ship!")
    if UserHits==5:
        print("You Won!!!")
        break
    
        
        
        
    
