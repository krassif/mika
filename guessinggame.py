import random
num=random.randint(1,100)
guess=num+100
guesses=0
while(not num==guess):
    guess=int(input('enter a guess'))
    if(guess < num):
        print('guess was too low')
    elif(guess > num):
        print('guess was too high')
    guesses=guesses+1
        
print("You guessed it!  Number was "+str(num))
print("You did it in "+str(guesses)+" guesse(s).")